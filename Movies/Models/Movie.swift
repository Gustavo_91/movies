//
//  Movie.swift
//  Movies
//
//  Created by Gustavo Pirela on 28/06/2019.
//  Copyright © 2019 Me. All rights reserved.
//

import Foundation

struct Movie: Decodable {
    
    let title: String
    let description: String?
    let voteAverage: Float
    let imagePath: String?
    
    private enum CodingKeys: String, CodingKey {
        case title
        case description = "overview"
        case voteAverage = "vote_average"
        case imagePath = "poster_path"
    }
    
}
