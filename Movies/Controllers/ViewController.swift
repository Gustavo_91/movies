//
//  ViewController.swift
//  Movies
//
//  Created by Gustavo Pirela on 27/06/2019.
//  Copyright © 2019 Me. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    let searchController = UISearchController(searchResultsController: nil)
    let networkClient = NetworkClient.shared
    var movies = [Movie]() {
        didSet {
            // Update UI
            print("Updating")
            collectionView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureSearchController()
        configureCollectionView()
    }
    
    // Mark: - Private
    
    private func configureSearchController() {
        searchController.searchBar.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search for Movies"
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = true
    }
    
    private func configureCollectionView() {
        collectionView.dataSource = self
        collectionView.delegate = self
        configureCollectionViewBackground(collectionView)
    }
    
    private func configureCollectionViewBackground(_ view: UICollectionView) {
        let backgroundView = UIView(frame: view.bounds)
        let gradientLayer = createBackgroundGradient(frame: view.bounds)
        backgroundView.layer.addSublayer(gradientLayer)
        view.backgroundView = backgroundView
    }
    
    private func createBackgroundGradient(frame: CGRect) -> CAGradientLayer {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = frame
        gradientLayer.colors = [UIColor(color: .purple).cgColor,
                                UIColor(color: .lightGray).cgColor,
                                UIColor(color: .magenta).cgColor]
        gradientLayer.contentsScale = UIScreen.main.scale
        gradientLayer.type = .axial
        gradientLayer.locations = [0.0, 0.3, 1.0]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.zPosition = -1
        return gradientLayer
    }
}

extension ViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text else { return }
        networkClient.getMovies(query: text) { movies in
            self.movies = movies
        }
    }
    
}

extension ViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieCell", for: indexPath) as! MovieCell
        configureMovieCell(cell, row: indexPath.row)
        return cell
    }
    
    private func configureMovieCell(_ cell: MovieCell, row: Int) {
        networkClient.getMovieImage(imagePath: movies[row].imagePath) { data in
            guard let image = UIImage(data: data) else { return }
            cell.moviePoster.image = image
        }
        cell.movieAverage.text = String(movies[row].voteAverage)
        cell.movieTitle.text = movies[row].title
        cell.movieDescription.text = movies[row].description
    }
    
}

extension ViewController: UICollectionViewDelegateFlowLayout {
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.bounds.size
    }
    
}



