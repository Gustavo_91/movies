//
//  UIColor.swift
//  Movies
//
//  Created by Gustavo Pirela on 01/07/2019.
//  Copyright © 2019 Me. All rights reserved.
//

import UIKit

extension UIColor {
    
    convenience init(red: Int, green: Int, blue: Int) {
        let red = (red >= 0 && red <= 255) ? red : 255
        let green = (green >= 0 && green <= 255) ? green : 255
        let blue = (blue >= 0 && blue <= 255) ? blue : 255
        self.init(red: CGFloat(red) / 255.0,
                  green: CGFloat(green) / 255.0,
                  blue: CGFloat(blue) / 255.0,
                  alpha: 1.0)
    }
    
    convenience init(hex: Int) {
        let red = (hex >> 16) & 0xff
        let green = (hex >> 8) & 0xff
        let blue = hex & 0xff
        self.init(red: red, green: green, blue: blue)
    }
    
    convenience init(color: MoviePalette) {
        self.init(hex: color.rawValue)
    }
    
}

enum MoviePalette: Int {
    case purple = 0x4B28CD
    case lightGray = 0xC2D1E7
    case magenta = 0xA709B3
}
