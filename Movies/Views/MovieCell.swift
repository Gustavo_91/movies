//
//  MovieCell.swift
//  Movies
//
//  Created by Gustavo Pirela on 29/06/2019.
//  Copyright © 2019 Me. All rights reserved.
//

import UIKit

class MovieCell: UICollectionViewCell {
    
    @IBOutlet weak var moviePoster: UIImageView!
    @IBOutlet weak var movieAverage: UILabel!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var movieDescription: UILabel!
    
}
