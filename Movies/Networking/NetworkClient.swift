//
//  Network.swift
//  Movies
//
//  Created by Gustavo Pirela on 28/06/2019.
//  Copyright © 2019 Me. All rights reserved.
//

import Foundation

class NetworkClient {
    
    static let shared = NetworkClient()
    
    func getMovies(query: String, completion: @escaping ([Movie]) -> ()) {
        guard let url = createMoviesUrl(with: query) else {
            return
        }

        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else {
                // TODO: Handle request error
                return
            }
            
            do {
                let results = try JSONDecoder().decode(MoviesDTO.self, from: data)
                DispatchQueue.main.async {
                    completion(results.movies)
                }
            } catch {
                // TODO: Handle data parser errors
            }
        }.resume()
    }
    
    func getMovieImage(imagePath path: String?, completion: @escaping (Data) -> ()) {
        guard let path = path, let url = createMoviePosterUrl(withPath: path) else {
            return
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard let data = data else {
                // TODO: Handle request error
                return
            }
            
            DispatchQueue.main.async {
                completion(data)
            }
        }.resume()
    }
    
    // MARK: - Private Methods
    
    private init() {}
    
    private func createMoviesUrl(with query: String) -> URL? {
        let queryItems = createMoviesQueryItems(with: query)
        let components = createMoviesUrlComponents(with: queryItems)
        return components?.url
    }
    
    private func createMoviesQueryItems(with query: String) -> [URLQueryItem] {
        let queryItems = [
            URLQueryItem(name: Constants.apiKeyParameter, value: Constants.apiKey),
            URLQueryItem(name: Constants.queryParamer, value: query),
        ]
        
        return queryItems
    }
    
    private func createMoviesUrlComponents(with items: [URLQueryItem]) -> URLComponents? {
        var components = URLComponents(string: Constants.baseMovieUrl)
        components?.queryItems = items
        return components
    }
    
    private func createMoviePosterUrl(withPath path: String) -> URL? {
        var url = URL(string: Constants.basePosterPath)
        url?.appendPathComponent(path.replacingOccurrences(of: "/", with: ""))
        return url
    }
    
    // MARK: - Private Data
    
    private struct MoviesDTO: Decodable {
        let movies: [Movie]
        
        private enum CodingKeys: String, CodingKey {
            case movies = "results"
        }
    }
    
    private struct Constants {
        static let apiKeyParameter = "api_key"
        static let queryParamer = "query"
        static let apiKey = "37770596e3c5abb9fcfdf150e446f55f"
        static let baseMovieUrl = "https://api.themoviedb.org/3/search/movie"
        static let basePosterPath = "https://image.tmdb.org/t/p/w300"
    }
}


