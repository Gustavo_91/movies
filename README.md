# Movies App

An iOS application that uses search keywords to show movies to the user

## Getting Started

Checkout master or develop branch

### Prerequisites

* Xcode 10.2 or superior
* Swift 5

### Notes

Unit tests have not been done for this project because it was a test project with a short time for its implementation and a very short life cycle, but if you think that the tests are necessary or mandatory to evaluate me in a more effective way, please tell me and I will do it

## Authors

* **Gustavo Pirela**